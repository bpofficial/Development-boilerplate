# Development-boilerplate
Boilerplate for:
    * React         (Front end)
    * Express       (Back end)
    * Apollo        (API)
    * GraphQL       (API)
    * Webpack       (Bundler)
    * Typescript    (Language)
    * Storybook     (Utility)
    * Cypress       (Testing)
    * ESLint        (Linting)
    * Custom scripts

Run 
`npm install`
Followed by
`npm start`

Check package.json for other scripts & usage.