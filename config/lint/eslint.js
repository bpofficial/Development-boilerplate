module.exports = {
    env: {
        browser: true,
        es6: true,
    },
    extends: [
        "standard",
        "eslint:recommended",
        "plugin:react/recommended",
        "eslint:recommended", 
        "plugin:@typescript-eslint/recommended"
    ],
    globals: {
        Atomics: "readonly",
        SharedArrayBuffer: "readonly"
    },
    parser: "typescript-eslint-parser",
    parserOptions: {
        ecmaFeatures: {
            jsx: true
        },
        project: "C:\\Users\\Brayden\\Desktop\\Web\\Testing Etc\\Full Dev\\config\\typescript\\tsconfig.js",
        ecmaVersion: 2018,
        sourceType: "module"
    },
    plugins: [
        "react",
        "react-hooks"
    ],
    rules: {
        "react-hooks/rules-of-hooks": "error",
        "react-hooks/exhaustive-deps": "warn",
        "no-console": "off",
    }
}