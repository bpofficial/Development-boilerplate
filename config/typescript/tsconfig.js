module.exports = {
    compilerOptions: {
        outDir: "../../dist/",
        sourceMap: true,
        removeComments: true,
        allowJs: false,
        moduleResolution: "node",
        module: "commonjs",
        target: "es6",
        jsx: "react",
        allowSyntheticDefaultImports: true,
        lib: ["es5", "es6", "es7", "es2017", "dom"],
        declaration: true,
        declarationDir: "../../extra/types/",
        forceConsistentCasingInFileNames: true,
        noImplicitReturns: true,
        noImplicitThis: true,
        noImplicitAny: true,
        strictNullChecks: true,
        suppressImplicitAnyIndexErrors: true,
        noUnusedLocals: true,
        rootDirs: ["../../src", "../../extra/stories"],
    },
    include: [
        "../../src/**/*"
    ],
    exclude: [
        "../../node_modules",
        "../../build",
        "../../config"
    ]
}