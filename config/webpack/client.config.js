const path = require('path');
const webpack = require('webpack');

module.exports = { 
    entry: {
        client: path.join( __dirname, '../../src/client/index.tsx' ) 
    },
    devtool: 'source-map',
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx']
    },
    output: { 
       path: path.resolve('build') + '/Release', 
       filename: 'client.bundled.js' 
    }, 
    module: { 
        rules: [
            {
                test: /\.(ts|tsx)$/,
                exclude: /node_modules/, 
                use: [
                    {
                        loader: "awesome-typescript-loader?configFileName=./config/typescript/tsconfig.json"
                    },
                    {
                        loader: require.resolve('react-docgen-typescript-loader'),
                    },
                ]
            }, 
            { 
                test: /\.css$/, 
                use: ["style-loader", "css-loader"] 
            }, 
            { 
                enforce: "pre", 
                test: /\.(js|jsx)$/, 
                loader: "source-map-loader" 
            }
        ] 
    }, 
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ] 
} 
