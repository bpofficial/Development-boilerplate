const path = require('path') 
//const webpack = require('webpack') 
const nodeExternals = require('webpack-node-externals') 
module.exports = { 
    entry: { 
        server: path.join( __dirname, '../../src/server/' ) 
    }, 
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx']
    },
    output: { 
        path: path.resolve('build') + '/Release', 
        publicPath: '/', 
        filename: '[name].bundled.js',
        sourceMapFilename: '[name].js.map'
    }, 
    target: 'node', 
    node: { 
        // Need this when working with express, otherwise the build fails 
        __dirname: false,   // if you don't put this is, __dirname 
        __filename: false,  // and __filename return blank or / 
    }, 
    externals: [nodeExternals()], // Need this to avoid error when working with Express 
    module: { 
        rules: [ 
        { 
            test: /\.(ts|tsx)$/,
            exclude: /node_modules/, 
            use: { 
                loader: "awesome-typescript-loader?configFileName=./config/typescript/tsconfig.json" 
            } 
        }] 
    } 
} 
