import * as React from 'react';
import { mount } from 'enzyme';
import PageContainer from '../../src/client/components/presentational/PageContainer/PageContainer';
import { expect } from 'chai';

const enzyme = require("enzyme");
const Adapter = require("enzyme-adapter-react-16");
enzyme.configure({ adapter: new Adapter() })

// Describe the test
describe( 'PageContainer Test Suite' , () => {
    it('should have hello as the id.', () => {
        const check = 'hello'
        const wrapper = mount(<PageContainer title={check} />);
        expect(wrapper.getDOMNode().getAttribute('id')).to.equal(check)
    });

    it( 'should not have an id attribute' , () => {
        const wrapper = mount(<PageContainer title={null} />);
        expect(wrapper.getDOMNode().getAttribute('id')).to.equal(null);
    })
});
