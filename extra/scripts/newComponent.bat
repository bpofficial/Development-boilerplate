@echo off
cd ..
cd ..
set /p NEW_COMP=Name your component (PamelCase): 
set /p Ext=Set the file extension(.js, .ts, etc.): .
mkdir "%CD%"\src\client\components\%NEW_COMP%

echo import * as React from 'react'; > "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.%Ext%
echo. >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.%Ext%
echo interface %NEW_COMP%Props { >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.%Ext%
echo    // Put the types here :) >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.%Ext%
echo }  >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.%Ext%
echo. >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.%Ext%
echo export default class %NEW_COMP% extends React.Component^<%NEW_COMP%Props, {}^> { >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.%Ext%
echo    public render(): React.ReactElement { >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.%Ext%
echo       return(^<div^>%NEW_COMP%^</div^>) >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.%Ext%
echo    } >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.%Ext%
echo } >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.%Ext%

echo import { mount } from 'enzyme'; > "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.spec.%Ext%
echo import * as React from 'react'; >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.spec.%Ext%
echo import %NEW_COMP% from './%NEW_COMP%'; >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.spec.%Ext%
echo. >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.spec.%Ext%
echo // Describe the test suite >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.spec.%Ext%
echo describe( '%NEW_COMP% Test Suite', () =^> { >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.spec.%Ext%
echo    it( 'Should exist.', () =^> { >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.spec.%Ext%
echo        const wrapper = mount( ^<%NEW_COMP% /^> ); >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.spec.%Ext%
echo        expect( wrapper.getDOMNode() ).toBeTruthy(); >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.spec.%Ext%
echo    }); >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.spec.%Ext%
echo }) >> "%CD%"\src\client\components\%NEW_COMP%\%NEW_COMP%.spec.%Ext%

echo import * as React from 'react'; > "%CD%"\extra\stories\%NEW_COMP%.js
echo import { storiesOf } from '@storybook/react'; >> "%CD%"\extra\stories\%NEW_COMP%.js
echo import %NEW_COMP% from '../../src/client/components/%NEW_COMP%/%NEW_COMP%'; >> "%CD%"\extra\stories\%NEW_COMP%.js
echo. >> "%CD%"\extra\stories\%NEW_COMP%.js
echo storiesOf('%NEW_COMP%', module) >> "%CD%"\extra\stories\%NEW_COMP%.js
echo .add('Default', () => ( >> "%CD%"\extra\stories\%NEW_COMP%.js
echo    <%NEW_COMP% /> >> "%CD%"\extra\stories\%NEW_COMP%.js
echo ), { info: { inline: true }}); >> "%CD%"\extra\stories\%NEW_COMP%.js