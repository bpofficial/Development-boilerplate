import * as React from 'react';
interface PageContainerProps {
    title?: string | null;
}
export default class PageContainer extends React.Component<PageContainerProps, {}> {
    render(): React.ReactElement;
}
export {};
