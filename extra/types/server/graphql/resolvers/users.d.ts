interface IO {
    [index: string]: string;
}
declare const _default: {
    Query: {
        user: (_root: string, args: IO) => Promise<object>;
        users: () => Promise<object>;
    };
    Mutation: {
        addUser: (_root: string, { name, email, level }: IO) => Promise<object>;
        editUser: (_root: string, { id, name, email }: IO) => Promise<object>;
        deleteUser: (_root: string, args: IO) => Promise<object>;
    };
};
export default _default;
