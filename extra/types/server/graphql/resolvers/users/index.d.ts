declare const _default: {
    Query: {
        user: (_root: any, args: any) => Promise<{}>;
        users: () => Promise<{}>;
    };
    Mutation: {
        addUser: (root: any, { id, name, email }: any) => Promise<{}>;
        editUser: (_root: any, { id, name, email }: any) => Promise<{}>;
        deleteUser: (_root: any, args: any) => Promise<{}>;
    };
};
export default _default;
