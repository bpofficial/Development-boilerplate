import one from './Theme1';
import two from './Theme2';
import three from './Theme3';

export default {
    one,
    two,
    three
}