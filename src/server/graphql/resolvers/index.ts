import { mergeResolvers } from "merge-graphql-schemas";

import User from "./users";

const resolvers = [
    User
];

export default mergeResolvers(resolvers);