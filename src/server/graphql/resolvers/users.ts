import User from "../../models/user.mdl";
const ObjectID = require('mongodb').ObjectID;

interface IO {
    [index: string]: string;
}

export default {
    Query: {
        user: ( _root: string, args: IO ): Promise<object> => {
            return new Promise<object>( ( resolve: Function, reject: Function ): void => {
                if( 'id' in args ) args = { ...args, _id: ObjectID( args.id ) }; delete args.id
                User.findOne ( args ).exec( ( err: Error, res: IO ): void => {
                    console.log(typeof res, res)
                    err ? reject( err ) : resolve( res );
                });
            });
        },
        users: ( ): Promise<object> => {
            return new Promise<object>( ( resolve: Function, reject: Function ): void => {
                User.find().exec( ( err: Error, res: IO ): void => {
                    console.log(typeof res, res)
                    err ? reject( err ) : resolve( res );
                });
            });
        }
    },
    Mutation: {
        addUser: ( _root: string, { name, email, level }: IO ): Promise<object> => {
            const newUser = new User( { name, email, level } );
            return new Promise<object>( ( resolve: Function, reject: Function ): void => {
                newUser.save( ( err: Error, res: any ): void => {
                    console.log(typeof res, res, err)
                    err ? reject( err ) : resolve( res );
                });
            });
        },
        editUser: ( _root: string, { id, name, email }: IO ): Promise<object> => {
            return new Promise<object>( (resolve: Function, reject: Function ): void => {
                User.findOneAndUpdate( { _id: ObjectID( id ) }, { $set: { name, email } } ).exec(
                    ( err: Error, res: IO ): void => {
                        err ? reject( err ) : resolve( res );
                    }
                );
            });
        },
        deleteUser: ( _root: string, args: IO ): Promise<object> => {
            return new Promise<object>( ( resolve: Function, reject: Function ): void => {
                if( 'id' in args ) args = { ...args, _id: ObjectID( args.id ) }; delete args.id
                User.findOneAndRemove( args ).exec( ( err: Error, res: IO ): void => {
                    err ? reject( err ) : resolve( res );
                });
            });
        }
    }
};