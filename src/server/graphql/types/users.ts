export default `
  type User {
    id: String!
    name: String!
    email: String!
    token: String!
    level: String!
  }
  type Query {
    user(id: String!): User
    users: [User]
  }
  type Mutation {
    addUser(name: String!, email: String!, level: String!): User
    editUser(id: String, name: String, email: String): User
    deleteUser(id: String, name: String, email: String): User
  }
`;