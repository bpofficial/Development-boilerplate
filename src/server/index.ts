import * as bodyParser from 'body-parser'; 
import * as express from 'express';
import * as path from 'path';
import * as pino from 'express-pino-logger';
import * as mongoose from 'mongoose';
import * as cors from 'cors';
import { ApolloServer } from 'apollo-server-express';
import GQSchema from './graphql/';
import { GraphQLError } from 'graphql';

try {
    require('dotenv').config({ path: path.resolve('config') + '/env/.development.env' });

    /**
     * Intialise App & Middleware
     */
    const app = express(); 
    app.use( cors() );
    app.use( bodyParser.json() ); 
    app.use( pino() ); 
    app.use( express.static ( path.resolve( __dirname + 'dist' ) ) ); 
 
    /**
     * Intialiase Apollo GraphQL
     */
    new ApolloServer({ 
        schema: GQSchema,
        playground: process.env.mode == 'dev' ? true : false,
        formatError: (err): GraphQLError => {
            // Don't give the specific errors to the client.
            if ( err.message.startsWith( "Database Error: " ) ) {
                return new GraphQLError( 'Internal server error [DB]' );
            } else {
                return new GraphQLError( err.message );
            }
        }
    }).applyMiddleware({ 
        app, 
        path: '/api',
    });

    /**
     * Respond with nothing to a direct uri connection
     */
    app.get( '/', function( _req: express.Request, res: express.Response ): void { 
        res.status(200).send().end() 
    }); 

    try {
        /**
         * Wrap server intialisation in a database connection callback.
         *  In the event the connection fails, the server won't start.
         */
        mongoose.connect( 
            'mongodb://' + process.env.DB_HOST + ':' + process.env.DB_PORT + '/' + process.env.DB_TABLE, 
            { useCreateIndex: true, useNewUrlParser: true }, 
            ( dbErr: Error ): void => {
                if ( dbErr ) throw dbErr; else console.log('Connected to MongoDB: [' + process.env.DB_HOST + ':' + process.env.DB_PORT + '/' + process.env.DB_TABLE + ']')
                try {
                    app.listen( process.env.SERVER_PORT, ( svrErr: Error ): void => {
                        if ( svrErr ) throw svrErr; else console.log( 'Server is running at ' + process.env.SERVER_URL + ':' + process.env.SERVER_PORT )
                    }); 
                } catch ( Err ) {
                    console.log( 'Failed to start server: ' + Err );
                }
            }
        );
    } catch ( Err ) {
        console.log( 'Failed to connect to database: ' + Err ) 
    }
} catch ( Err ) {
    console.log('Failed to initialise server.')
} 

