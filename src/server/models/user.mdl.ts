import * as mongoose from "mongoose";

const UserSchema = new mongoose.Schema({
    id: {
        type: mongoose.Schema.Types.ObjectId,
        required: false,
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        index: true,
        auto: false
    },
    token: {
        type: String,
        required: false
    },
    level: {
        type: String,
        required: true
    }
});

export default mongoose.model("user", UserSchema);